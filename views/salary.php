<?php
	require "../classes/Database.php";
	require "../classes/General.php";
	require "../classes/HTMLPage.php";
	require "../classes/Employees.php";
	
	$h = new HTMLPage;
	echo $h->head();
	echo $h->bodystart();
	echo $h->searchTool();
?>
<!-- This view presents Employees sorted by Salary  -->
<div class="content-wrapper">
    <section class="content">
    	<div class="card">
        	<div class="card-body">
  				<center><h3 class="text-info">Data filtered by salary</h3></center>	
				<?php
					$employees = new Employees();
					$salarySort = $employees->sortBySalary();					
				?>
				<div id="gantt_here" style='width:100%; height:400px;'>
					<table class="table">
						<thead>
							<th><a href='empNo.php'>EMP_NO</a></th>					
							<th><a href='birthdate.php'>BIRTHDATE</a></th>					
							<th><a href='firstname.php'>FIRST_NAME</a></th>					
							<th><a href='lastname.php'>LAST_NAME</a></th>					
							<th><a href='gender.php'>GENDER</a></th>					
							<th><a href='hiredate.php'>HIRE_DATE</a></th>					
							<th><a href='deptName.php'>DEPT_NAME</a></th>
							<th><a href='title.php'>TITLE</a></th>
							<th><a href='salary.php'>SALARY</a></th>
						</thead>
						<tbody>
							<?php
								foreach($salarySort as $key=>$row){
									foreach($row as $k=> $cell){
										switch ($k) {											
											case "color":
												$color="<span class='badge badge-".$cell ."'>";
												break;
											default:
												echo "<td>$cell</td>";
												break;
										}
										if($k=="emp_no"){
											$id=$cell;
										}
									}
									
									echo "</tr>";
								}
								
							?>
						</tbody>
				</table>
				</div>

    	    </div>
	    </div>
    </section>
</div>


