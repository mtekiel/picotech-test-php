<?php
class HTMLPage{
   public $pageTitle = "Filter / Search data in PHP - Picotech test";

   function head(){
		$head="<!DOCTYPE html>
		<html>
		<head>
			<meta charset='utf-8'>
			<meta http-equiv='X-UA-Compatible 'content='IE=edge'>
			<title>Filter/Search data in PHP</title>
			<meta name='viewport' content=width='device-width, initial-scale=1'>
			<link rel='stylesheet' href='plugins/fontawesome-free/css/all.min.css'>
			<link rel='stylesheet' href='styles/main.css'>
			<link rel='stylesheet' href='../styles/bootstrap.min.css'>
			<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
			<link rel='stylesheet' href='dist/css/adminlte.min.css'>
			<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700' rel='stylesheet'>

			<link rel=\"stylesheet\" href=\"plugins/gantt/skins/dhtmlxgantt_meadow.css\">
		</head>";
		return $head;
   }

   function bodystart(){
		$b="<body class='hold-transition sidebar-mini layout-fixed'>
		<div class='wrapper'>";
		return $b;
   }   

   function searchTool2(){
		$s = "<form action='search.php' method='POST'>
				<div class='input-group mb-3'>
					<input type='text' class='form-control' name='search' placeholder='Search'>	
					<div class='input-group-append'>			
						<a type='submit' name='submit-search' class='btn btn-info'>Submit</a>	
					</div>	
				</div>				
			 <form>";
			 
		return $s;
   }  

   function searchTool(){
		$s = "<form action='search.php' method='POST'>
			<div class='input-group mb-3'>
				  <input type='text' class='form-control' name='search' placeholder='Search'>
			</div>
			  	<input class='btn btn-primary' type='submit' value='Submit'>
			 <form>";
			 
		return $s;
   }
     
}


