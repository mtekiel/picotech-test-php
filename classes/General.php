<?php
class General{

    function __construct(){
        $this->db = new Database();
        $this->db->connect();
    } 
  	  
	function newbutton($text,$link,$color){
		$b="<a href='$link' class='btn btn-$color'>$text</a>";
		return $b;
	}
	
	function color($v){
		$opt="";
		$color = array('primary' ,'info' ,'danger','warning', 'success', 'secondary');
		
		foreach($color as $key =>$value){
			if($v==$value) $s="selected"; else  $s="";
			$opt.="<option value='$value' $s>".ucfirst($value)."</option>";
		}
		return $opt;
  	}
	
}
