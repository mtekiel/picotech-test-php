<?php
class Employees{
	private $db;
	
    function __construct(){
        $this->db = new Database();
        $this->db->connect();
	}
	
    function getAll(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no", "select") ;
      	return $do;    	
	}
	
	function sortByEmpNo(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.emp_no ASC", "select") ;
      	return $do;    	
	}
    
	function sortByBirthdate(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.birth_date ASC", "select") ;
      	return $do;    	
	}

	function sortByFirstname(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.first_name ASC", "select") ;
      	return $do;    	
	}

	function sortByLastname(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.last_name ASC", "select") ;
      	return $do;    	
	}

	function sortByGender(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.gender ASC", "select") ;
      	return $do;    	
	}

	function sortByHiredate(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY e.hire_date ASC", "select") ;
      	return $do;    	
	}

	function sortByDeptName(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY d.dept_name ASC", "select") ;
      	return $do;    	
	}

	function sortByTitle(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY t.title ASC", "select") ;
      	return $do;    	
	}

	function sortBySalary(){
      	$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no ORDER BY s.salary ASC", "select") ;
      	return $do;    	
	}

	function search($search){
		$do= $this->db->query("SELECT e.emp_no, DATE_FORMAT(birth_date, '%d/%m/%Y'),first_name, last_name, gender, DATE_FORMAT(hire_date, '%d/%m/%Y'), d.dept_name, t.title, s.salary
			FROM employees as e, dept_emp as e_d, departments as d, titles as t, salaries as s
			WHERE (e.emp_no LIKE '%$search%' OR e.first_name LIKE '%$search%' OR e.last_name LIKE '%$search%' OR e.gender LIKE '%$search%' OR d.dept_name LIKE '%$search%' OR t.title LIKE '%$search%') AND e.emp_no = e_d.emp_no AND d.dept_no = e_d.dept_no AND t.emp_no = e.emp_no AND s.emp_no=e.emp_no", "select") ;
      	return $do; 
	}
}

